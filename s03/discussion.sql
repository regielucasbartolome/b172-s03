/*Inserting Records*/
--Command -> INSERT INTO tbl_name(column) VALUES ("col_value");

--Insert values in artists table
INSERT INTO artists(name) VALUES ("Rivermaya");
INSERT INTO artists(name) VALUES ("Psy");

--Insert values in albums table
--MySQL Year Format: YYY-MM-DD
INSERT INTO albums(album_title, date_released, artists_id) VALUES ("Psy 6", "2012-1-1", 2);
INSERT INTO albums(album_title, date_released, artists_id) VALUES ("Trip", "1996-1-1", 1);

--Insert values into songs table
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-POP", 1);
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kisapmata", 279, "OPM", 2);

--Selecting Records

--Display the title and genre of all the songs
SELECT song_name, genre FROM songs;

--Displat all columns in the songs table
SELECT * FROM songs;

--Display the title of all the OPM songs
SELECT * FROM songs WHERE genre = "OPM";

--We can use AND and OR keyword for multiple expressions in the WHERE clause.

--Display the title and length of the OPM songs that are more than 4 minutes.
SELECT song_name, length FROM songs WHERE genre = "OPM" AND length < 2;

--Update the length of Kundiman to 240 seconds.
UPDATE songs SET length = 240 WHERE song_name = "Kundiman"; 


UPDATE songs SET length = 250 WHERE song_name = "Kisapmata";
UPDATE songs SET length = 200 WHERE song_name = "Gangnam Style";

--Deleting Records

--Delete all OPM songs that >= 2 minutes
DELETE FROM songs WHERE genre = "OPM" AND length >= 200;

--DELETE all rows in songs table by removing the WHERE keyword
DELETE FROM songs;